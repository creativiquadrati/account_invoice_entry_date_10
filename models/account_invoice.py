# =============================================================== #
#//Eredito AccountInvoice
#//Aggiungo il campo registration_date
#//Considerazioni sul porting di questa funzione: i periodi fiscali non esistono piu quindi non
#//saranno aggiornati nella registrazione di sezionale
# =============================================================== #

from odoo import models,api,fields,_
from datetime import date
from odoo.exceptions import UserError

class AccountInvoice(models.Model):
    _inherit='account.invoice'
    
    # ====================================== #
    #            CAMPI Aggiunti              #
    # ====================================== #

    # registration_date: redundant field (some role as native "date" field),
    # from now on preserved just for retro-compatibility with dependent modules
    # (eg: cq_invoice_10) and databases where it has already been filled.
    # Every time "date" will change, it will trigger registration_date changing
    registration_date = fields.Date(
        string="Registration Date",
        states={'paid': [('readonly', True)],'open': [('readonly', True)],'close': [('readonly', False)],},
        help="Keep empty to use the current date")

    @api.model
    def create(self, vals):
        '''
            @summary: Questa funzione associa alla registration date
            la data corretta sia se si tratta di una fattura fornitore
            che di una cliente, inoltre associa il sezionale alla posizione
            fiscale
        '''
        Journal = self.env['account.journal']
        if vals.get('journal_id', False):
            journal_id = vals['journal_id']
            journal = Journal.browse(journal_id)
        else:
            journal = False

        if journal and journal.type == 'purchase':
            if not vals.get('date', False):
                vals['date'] = date.today()
                vals['registration_date'] = vals['date']
            else:
                vals['registration_date'] = vals['date']

        # ========================================================== #
        #        Sezionale di default sulla posizione fiscale        #
        # viene importato solo se la fattura e creata da un SO       # 
        # ========================================================== #

        fpos = vals.get('fiscal_position_id',)
        fatttype = vals.get('type', False)
        if fpos and fatttype in ('out_invoice', 'out_refund'):
            journal = self.env['account.fiscal.position'].browse(fpos).inv_journal
            if journal:
                vals['journal_id'] = journal.id
        res = super(AccountInvoice, self).create(vals)
        return res


    @api.multi
    def write(self, vals):

        if 'date' in vals:
            vals['registration_date'] = vals.get('date')
        res = super(AccountInvoice, self).write(vals)

        return res


    @api.multi
    def action_invoice_draft(self):
        '''
            @note: Questa funzione viene utilizzata nel
            caso in cui: 
            1)  La fattura sia una fattura cliente o una nota di
                credito (in quanto le fatture fornitori o le note
                di credito fornitore possono essere modificate 
                manualmente).
            2)  La data di registrazione della fattura al momento
                della convalida prende la data odierna.
            3)  Se la fattura viene poi reimpostata a bozza la data
                di registrazione rimane quella, se poi la data fattura
                viene incrementata e si cerca di convalidare nuovamente
                la fattura. C'e il blocco dovuto al fatto che la data 
                di registrazione e maggiore della data della fattura
            @summary: Questa funzione riporta quindi la data di
            registrazione nello stato False in modo che, dopo la successiva
            convalida il tutto sia in stato corretto.
        '''
        res = super(AccountInvoice,self).action_invoice_draft()
        if self.type in ('out_invoice','out_refund'):
            self.date = False
        return res

    @api.multi
    def action_move_create(self):

        res = super(AccountInvoice,self).action_move_create()

        for inv in self:
            # ================================================= #
            #    Recupero date_invoice e registration_date      #
            # ================================================= #

            date_invoice = inv.date_invoice or False
            date = inv.date or False
            if not date:
                if not date_invoice:
                    date = date.strftime(date.today(),'%Y-%m-%d')
                else:
                    date = date_invoice
            # ================================================= #
            #        Controllo che le date siano corrette       #
            # ================================================= #

            if date_invoice and date:
                if (date_invoice > date):
                    raise UserError(
                        _("Invoice Date (%s) cannot be later than Accounting Date (%s)!")
                        % (date_invoice, date)
                    )

            inv.write({'date' : date})
            move_date = date or date_invoice or date.strftime(date.today(),'%Y-%m-%d')

            # ======================================================= #
            #    Aggiorno il registro sezionale e le voci sezionale   # 
            #    collegate (account.move.line)  con la nuova data     #
            # ======================================================= #

            inv.move_id.write({'state': 'draft'})
            self._cr.execute('''UPDATE account_move_line
            SET date = %s
            WHERE move_id =%s
            ''', (move_date,inv.move_id.id))
            
            inv.move_id.write({'date' : move_date})
            inv.move_id.write({'state' : 'posted'})

        return res
